---
layout: page
title: Bakara
description: Sûre
excerpt: "2'ncü Sûre"
modified: 2020-05-27T17:50:00.564948 17:00
encoding: UTF-8
tag: 
 - Bakara
---

## 2. Bakara Sûresi

**(1)** Elif Lâm Mîm
**(2)** işte o Kitap kendisinde hiç şüphe yoktur müttakiler için yol göstericidir
**(3)** onlar ki gaybde(gizlide) inanırlar ve namazlarını kılarlar ve kendilerini rızıklandırdığımız şeyden infak ederler
**(4)** ve onlar ki sana indirilen şeye ve senden önceden indirilen şeye iman ederler ve ahirete de onlar kesinlikle inanırlar
**(5)** işte onlar Rablerinden bir hidayet üzeredirler ve umduklarına erenler işte onlardır
**(6)** elbette inkar edenler onlara eşittir ki onları uyarman yada uyarmasan da inanmazlar
**(7)** Allah kalblerinin üzerini ve kulaklarının üzerini mühürlemiştir ve gözlerinin üzerine perde inmiştir Onlar için büyük bir azab vardır 
**(8)** ve onlar ahiret inanıyor olmadıkları halde insanlardan öyleleri de Allah’a ve ahiret gününe inandık derler 
**(9)** Allah’ı ve inanan kimseleri aldatmağa çalışırlar kendilerinden başkasını aldatamazlar farkında değiller
**(10)** onların kablerinde hastalık vardır Allah hastalıklarını artırmıştır yalancı olduklarından ötürü onlara acı bir azab vardır
**(11)** onlara yeryüzünde bozgunculuk yapmayın denildiği zaman biz sadece düzelticileriz derler
**(12)** İyi bilin ki muhakkak onlar bozgunculardır fakat anlayanlardan değildir
**(13)** onlara insanların inandıkları gibi iman edin denildiği zaman beyinsizlerin inandığı gibi inanır mıyız? derler iyi bilin ki asıl beyinsizler [akılsızlık edip sonunu düşünmeden hazz-ı nefis için masraf edenler] doğrusu onlardır fakat onlar [ilim sahibi] bilenlerden değildir
**(14)** inanan kimselere rastladıkları zaman inandık derler ve şeytanları ile yalnız kaldıkları zaman şüphesiz biz sizinle beraberiz derler elbette sadece biz (onlarla) alay ediyoruz 
**(15)** Allah kendileriyle alay eder ve taşkınları içinde onları bırakır bocalayıp dururlar
**(16)** işte onlar hidayet karşılığında sapıklığı satın aldılar ticaretleri kâr etmedi ve doğru yolu bulan olanlardan değildir
**(17)** Onların durumu ateş yakan kişinin durumu gibidir ne zaman ki çevresini aydınlatır Allah onların nurunu giderdi ve onları karanlıklar içinde bıraktı görenlerden değildir
**(18)** sağırdırlar dilsizdirler kördürler onlar dönecek değildir
**(19)** ya da (onlar) gök gürlemesi ve şimşek (ler) ve karanlıklar içinde gökten boşanan yağmur gibi yıldırım seslerinden ölüm korkusuyla kulakları içine parmaklarını tıkarlar oysa Allah inkarcıları tamamen kuşatmıştır
**(20)** neredeyse şimşek onları aydınlattığı zaman gözlerini kapıverecek üzerlerine karanlık çöktüğü zaman o(nun ışığı)nda yürürler dikilip kalırlar eğer Allah dileseydi elbette işitmelerini ve görmelerini götürürdü Şüphesiz Allah’ın her şey üzerine gücü yeter
**(21)** ey insanlar Rabbinize kulluk edin o ki; sizi yarattı ve o ki; sizden öncekileri [de] belki korunursunuz
**(22)** O (Rabb) ki sizin için yeri döşek ve göğü bina kıldı ve gökten su  indirdi onunla sizin için rızık olarak çeşitli ürünlerden çıkardı ve öyleyse siz de bile bile Allah’a (denk) eşler koşmayın 
**(23)** sana kulumuz (Muhammed)e indirdiğimizden eğer şüphe içinde iseniz haydi onun gibi bir sure getirin ve eğer doğru iseniz Allah'dan başka şahitlerinizi çağırın
**(24)** yok eğer yapmadınızsa ki asla yapamayacaksınız o halde ateşten sakının ki onun yakıtı inkarcılar için hazırlanmış insanlar ve taşlardır 
**(25)** ve inanan ve salih işler işleyen kimseleri müjdele muhakkak onlar için altlarından ırmaklar akan cennetler vardır onlardaki meyveden rızk olarak her rızıklandırıldıklarında Bu daha önceden rızıklandığımız şeydir derler ona benzer onlara verilmiştir Onlar için orada tertemiz eşler vardır ve onlar orada ebedi kalacaklardır
**(26)** bir sivrisineği [onun] gibi bir örneği hatta onun da üstünde olanı misal vermekten muhakkak Allah çekinecek değildir gerçekten inanan kimseler kesinlikle bilirler Rablerinden haktır o (gerçektir) ve inkar edenler ise derler ki bu misalle Allah neyi istedi [ve neyi] (kasdetti) onunla bir çoğunu saptırır ve onunla yine bir çoğunu yola getirir fasıklardan başkasını onunla saptırmaz
**(27)** onlar ki söz verip bağlandıktan sonrada Allah’a (verdikleri) sözü bozarlar ve Allah’ın kendisiyle birleştirmesini emrettiği şeyi keserler ve yeryüzünde bozgunculuk yaparlar ziyana uğrayanlar işte onlardır
**(28)** nasıl Allah’a inkar edersiniz siz ölüler iken O sizi diriltti sonra öldürecek sonra diriltecek sonra O’na döndürüleceksiniz
**(29)** O ki yeryüzünde ne varsa hepsini sizin için yarattı sonra göke yöneldi  yedi gök (olarak) onları düzenledi ve O her şeyi bilir
**(30)** bir zamanlar Rabbin meleklere dedi ki şüphesiz ben yeryüzünde bir halife yaratacağım (melekler) dediler orada kan döken orada bozgunculuk yapan kimse mi yaratacaksın? oysa biz seni [hamd ile] överek tesbih ediyor ve seni takdis ediyoruz şüphesiz dedi ben bilirim şeyleri [onları] siz biliyor değilsiniz
**(31)** ve meleklere onları sunup sonra bütün isimleri Adem’e öğretti ve eğer doğru kimseler iseniz onların isimlerini bana söyleyin dedi 
**(32)** dediler ki Seni tesbih ederiz bize öğrettiğin şeyden başka bizim bilgimiz yoktur şüphesiz sen [Alîmul Hakîm] sen bilensin hakim olansın
**(33)** (Allah) dedi ki Adem onların isimlerini bunlara haber ver ne zaman ki onların isimlerini bunlara haber verince (Allah) dedi ki size demiş değil miydim? şüphesiz ben göklerin ve yerin gayblarını bilirim ve sizin açıkladıklarınız şeyleri ve gizlemekte olduğunuz şeyleri bilirim
**(34)** hani Meleklere Adem’e secde edin demiştik İblis hariç hemen secde ettiler İblis kaçındı ve kibirlendi ve inkarcılardan oldu
**(35)** ve dedik ki Adem sen ve eşin cennette oturun ve dilediğiniz yerde ondan bol bol yeyin şu ağaca yaklaşmayın zalimlerden olursunuz
**(36)** şeytan onlar(ın ayağın)ı kaydırdı içinde bulundukları yerden oradan çıkardı ve dedik ki kiminiz kiminizen düşman olarak inin sizin için yeryüzünde kalmak ve bir süre nimet vardır
**(37)** derken Adem Rabbinden kelimeler aldı onun tevbesini kabul etti şüphesiz O [Tevvab Rahim] tevbeyi çok kabul edendir çok esirgeyendir 
**(38)** hepiniz oradan inin dedik benden bir hidayet size geldiği zaman kimler benim hidayetime uyarsa onlara artık [bir endişe] bir korku yoktur ve onlar [hüzne kapılmaz] üzülenlerden olmazlar
**(39)** ve inkar eden ve ayetlerimizi yalanlayan kimseler işte onlar ateş halkıdır onlar orada ebedi kalacaklardır
**(40)** İsrail oğulları ni’metleri hatırlayın o ki; sizleri ni’metlendirdim ve bana verdiğiniz sözü tutun size verdiğim sözü ben de tutayım ve sadece benden [çekinerek] korkun
**(41)** ve sizin yanınızda bulunanı doğrulayıcı olarak indirdiğim şeye inanın ve onu ilk inkar eden olmayın ve azıcık bedele benim ayetlerimi satmayın ve benden sakının
**(42)** ve batılla gerçeği katıştırmayın ve siz bildiğiniz halde hakkı gizlemeyin
**(43)** ve namazı kılın ve zekatı verin ve rüku edenlerle beraber ruku edin
**(44)** kendinizi unutuyorsunuz da insanlara iyiliği emir mi ediyorsunuz ve siz Kitabı okuduğunuz halde hâlâ aklınızı kullanmıyor musunuz?
**(45)** sabırla ve namazla yardım dileyin şüphesiz bu saygı gösterenlerden başkasına ağır gelir 
**(46)** onlar ki bilirler şüphesiz onlar Rablerine kavuşacaklardır ve gerçekten onlar O’na döneceklerdir
**(47)** İsrail oğulları ni’metimi hatırlayın ki sizi ni’metlendirdim ve şüphesiz alemler üzerine sizi [seçtim] üstün kıldım
**(48)** ve günden sakının kimseden(günahından) bir şey [için] hiç kimse cezalandırılmaz kimseden şefaat da kabul edilmez ve ondan fidye de alınmaz ve onlara hiçbir yardım yapılamaz
**(49)** hani Fir’avn ailesinden sizi kurtarmıştık onlar azabın en kötüsünü size reva görüyor oğullarınızı boğazlayıp kadınlarınızı sağ bırakıyorlardı ve bunda sizin için Rabbinizden büyük bir imtihan vardı
**(50)** hani sizin için denizi yarmıştık sizi kurtarmış ve Fir’avn ailesini boğmuştuk ve siz de görüyordunuz
**(51)** 
**(52)** 
**(53)** 
**(54)** 
**(55)** 
**(56)** 
**(57)** 
**(58)** 
**(59)** 
**(60)** 
**(61)** 
**(62)** 
**(63)** 
**(64)** 
**(65)** 
**(66)** 
**(67)** 
**(68)** 
**(69)** 
**(70)** 
**(71)** 
**(72)** 
**(73)** 
**(74)** 
**(75)** 
**(76)** 
**(77)** 
**(78)** 
**(79)** 
**(80)** 
**(81)** 
**(82)** 
**(83)** 
**(84)** 
**(85)** 
**(86)** 
**(87)** 
**(88)** 
**(89)** 
**(90)** 
**(91)**
**(92)** 
**(93)** 
**(94)**  
**(95)** 
**(96)** 
**(97)** 
**(98)** 
**(99)** 
**(100)** 
**(101)** 
**(102)** 
**(103)** 
**(104)** 
**(105)** 
**(106)** 
**(107)** 
**(108)** 
**(109)** 
**(110)** 
**(111)** 
**(112)** 
**(113)** 
**(114)** 
**(115)** 
**(116)** 
**(117)** 
**(118)** 
**(119)** 
**(120)** 
**(121)** 
**(122)** 
**(123)** 
**(124)** 
**(125)** 
**(126)** 
**(127)** 
**(128)** 
**(129)** 
**(130)** 
**(131)** 
**(132)** 
**(133)** 
**(134)** 
**(135)** 
**(140)** 
**(141)** 
**(142)** 
**(143)** 
**(144)** 
**(145)** 
**(146)** 
**(147)** 
**(148)** 
**(149)** 
**(150)** 
**(151)** 
**(152)** 
**(153)** 
**(154)** 
**(155)** 
**(156)** 
**(157)** 
**(158)** 
**(159)** 
**(160)** 
**(161)** 
**(162)** 
**(163)** 
**(164)** 
**(165)** 
**(166)** 
**(167)** 
**(168)** 
**(169)** 
**(170)** 
**(171)** 
**(172)** 
**(173)** 
**(174)** 
**(175)** 
**(176)** 
**(177)** 
**(178)** 
**(179)** 
**(180)** 
**(181)** 
**(182)** 
**(183)** 
**(184)** 
**(185)** 
**(186)** 
**(187)** 
**(188)** 
**(189)** 
**(190)** 
**(191)** 
**(192)** 
**(193)** 
**(194)** 
**(195)** 
**(196)** 
**(197)** 
**(198)** 
**(199)** 
**(200)** 
**(201)** 
**(202)** 
**(203)** 
**(204)** 
**(205)** 
**(206)** 
**(207)** 
**(208)** 
**(209)** 
**(210)** 
**(211)** 
**(212)** 
**(213)** 
**(214)** 
**(215)** 
**(216)** 
**(217)** 
**(218)** 
**(219)** 
**(220)** 
**(221)** 
**(222)** 
**(223)** 
**(224)** 
**(225)** 
**(226)** 
**(227)** 
**(228)** 
**(229)** 
**(230)** 
**(231)** 
**(232)** 
**(233)** 
**(234)** 
**(235)** 
**(236)** 
**(237)** 
**(238)** 
**(239)** 
**(240)** 
**(241)** 
**(242)** 
**(243)** 
**(244)** 
**(245)** 
**(246)** 
**(247)** 
**(248)** 
**(249)** 
**(250)** 
**(251)** 
**(252)** 
**(253)** 
**(254)** 
**(255)** 
**(256)** 
**(257)** 
**(258)** 
**(259)** 
**(260)** 
**(261)** 
**(262)** 
**(263)** 
**(264)** 
**(265)** 
**(266)** 
**(267)** 
**(268)** 
**(269)** 
**(270)** 
**(271)** 
**(272)** 
**(273)** 
**(274)** 
**(275)** 
**(276)** 
**(277)** 
**(278)** 
**(279)** 
**(280)** 
**(281)** 
**(282)** 
**(283)** 
**(284)** 
**(285)** 
**(286)** 

![02Bakara-Medeni]({{ site.baseurl }}/assets/images/ayrac-muhur.png "mühür")
